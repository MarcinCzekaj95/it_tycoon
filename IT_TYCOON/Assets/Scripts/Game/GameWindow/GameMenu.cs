﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameMenu : MonoBehaviour
{
    public Text playerDisplayInformation;
    // Update is called once per frame
    void Update()
    {
        playerDisplayInformation.text = "Player: " + DBManager.userName + " " +
            "Company: " + DBManager.comapnyName + " Money: " + DBManager.virtualWallet + "$";
    }
}
