﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EmplyeeGrid : MonoBehaviour
{
    public GameObject employee;
    public DisplayMenus menuManager;

    public List<Sprite> images;

    public int amountOfEmployees = 9;

    // Start is called before the first frame update
    void Start()
    {
        populate();
    }

    void populate()
    {
        GameObject newObj;
        for (int i = 0; i < amountOfEmployees; ++i)
        {
            newObj = (GameObject)Instantiate(employee, transform);
            newObj.GetComponent<Button>().image.sprite = images[i];
            newObj.GetComponent<EmployeeIndex>().index = i+1;

            newObj.GetComponent<Button>().onClick.AddListener(menuManager.openEmployeeHireMenu);

            newObj.name = "Employee" + i.ToString();

            menuManager.employees.Add(newObj);
        }
    }
}
