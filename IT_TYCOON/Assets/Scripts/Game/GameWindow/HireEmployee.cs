﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HireEmployee : MonoBehaviour
{
    public int employeeIndex;
    public EmployeeManager employeeManager;
    public Company company;
    public AudioSource buttonSound;
    public Button hireButton;
    public Button fireButton;
    public Text errorInformation;
    public float timer = 0.5f;

    void Update()
    {
        if (errorInformation.text != "")
        {
            timer -= 0.2f * Time.deltaTime;
            if (timer <= 0.0f)
            {
                timer = 0.5f;
                errorInformation.text = "";
            }
        }
    }

    public void callHireEmployee()
    {
        buttonSound.Play();
        bool employeeHired = employeeManager.employeesList.Exists(x => x.id == employeeIndex);
        if (!employeeHired)
        {
            if (DBManager.virtualWallet >= employeeManager.employee.value)
            {
                company.value += employeeManager.employee.value;
                StartCoroutine(hireEmployee());
            }
            else
            {
                errorInformation.text = "Cannot hire this employee because you don't have enough money";
                Debug.Log("Cannot hire this employee because you don't have enough money");
            }
        }
        else
        {
            errorInformation.text = "Cannot hire the same employee two times";
            Debug.Log("Cannot hire the same employee two times");
        }
    }

    IEnumerator hireEmployee()
    {
        WWWForm form = new WWWForm();
        form.AddField("Id", employeeIndex);
        form.AddField("CompanyName", DBManager.comapnyName);
        WWW www = new WWW("http://localhost:8080/sqlconnect/HireEmployee.php", form);
        yield return www;
        if (www.text[0] == '0')
        {
            string[] stringArray = www.text.Split('\t');
            string employeeName = stringArray[2];
            DBManager.virtualWallet -= employeeManager.employee.value;
            int id = employeeIndex - 1;
            var employee = GameObject.Find("Employee" + id);
            employee.GetComponent<Employee>().SetEmployeeValues(employeeManager.employee);
            employeeManager.employeesList.Add(employee.GetComponent<Employee>());
            errorInformation.text = "Congratulations you've hired new Employee: " + employeeName;
            Debug.Log("Congratulations you've hired new Employee: " + employeeName);
            hireButton.interactable = false;
            fireButton.interactable = true;
        }
        else
        {
            errorInformation.text = "Failed to hire Employee. ERROR #" + www.text;
            Debug.Log("Failed to hire Employee. ERROR #" + www.text);
        }
    }

    public void callFireEmployee()
    {
        buttonSound.Play();
        bool employeeHired = employeeManager.employeesList.Exists(x => x.id == employeeIndex);
        if (employeeHired)
        {
            StartCoroutine(fireEmployee());
        }
        else
        {
            errorInformation.text = "Cannot fire this employee";
            Debug.Log("Cannot fire this employee");
        }
    }

    IEnumerator fireEmployee()
    {
        WWWForm form = new WWWForm();
        form.AddField("Id", employeeIndex);
        form.AddField("CompanyName", DBManager.comapnyName);
        WWW www = new WWW("http://localhost:8080/sqlconnect/FireEmployee.php", form);
        yield return www;
        if (www.text[0] == '0')
        {
            string[] stringArray = www.text.Split('\t');
            string employeeName = stringArray[2];
            int id = employeeIndex - 1;
            var employee = GameObject.Find("Employee" + id);
            employeeManager.employeesList.Remove(employee.GetComponent<Employee>());
            Debug.Log("You've fired Employee: " + employeeName);
            hireButton.interactable = true;
            fireButton.interactable = false;
        }
        else
        {
            errorInformation.text = "Failed to fire employee. ERROR #" + www.text;
            Debug.Log("Failed to fire employee. ERROR #" + www.text);
        }
    }
}
