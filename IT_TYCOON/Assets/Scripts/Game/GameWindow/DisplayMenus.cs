﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DisplayMenus : MonoBehaviour
{
    public List<GameObject> employees;
    public Canvas hireEmployeesMenu;
    public Canvas improvementMenu;
    public Canvas gameOptionsMenu;
    public Canvas gameWindow;
    public Canvas hireMenu;
    public Canvas orderInfo;
    public OrdersMenu orderMenu;
    public HireEmployee hireEmployee;
    public EmployeeManager employeeManager;
    public UpdateCompany updateCompany;
    public Text orderInformations;
    public AudioSource buttonSound;
    public CheckIfEmployeedIsHired checkIfEmployeeIsHired;
    public Text errorInformationImprovement;
    public CommissionMenu commissionMenu;

    public List<Text> employeeInfoList;

    public void openCloseHireEmployeesMenu()
    {
        buttonSound.Play();
        hireEmployeesMenu.enabled = !hireEmployeesMenu.enabled;
    }

    public void OpenRankingPage()
    {
        Application.OpenURL("http://localhost:8080/sqlconnect/Ranking.html");
    }

    public void openImprovementMenu()
    {
        commissionMenu.canOpenOrderMenu = false;
        buttonSound.Play();
        improvementMenu.enabled = true;
    }

    public void closeImprovementMenu()
    {
        commissionMenu.canOpenOrderMenu = true;
        buttonSound.Play();
        improvementMenu.enabled = false;
    }

    public void upgradeCompanyToNextLevel()
    {
        buttonSound.Play();
        if (updateCompany.callCompanyLevel())
        {
            Debug.Log("Update company to next level");
            return;
        }
        errorInformationImprovement.text = "Cannot update company not enough money or company value is not enough";
        Debug.Log("You cannot update company");
    }

    public void openGameOptionsMenu()
    {
        buttonSound.Play();
        gameOptionsMenu.enabled = true;
        closeGameMenu();
    }

    public void closeGameOptionsMenu()
    {
        buttonSound.Play();
        gameOptionsMenu.enabled = false;
        openGameMenu();
    }

    private void openGameMenu()
    {
        buttonSound.Play();
        gameWindow.enabled = true;
    }

    private void closeGameMenu()
    {
        buttonSound.Play();
        gameWindow.enabled = false;
    }

    public void openOrderInfoMenu()
    {
        buttonSound.Play();
        orderMenu.selectedOrder = EventSystem.current.currentSelectedGameObject;
        orderInfo.enabled = true;
        orderInformations.text = orderMenu.selectedOrder.GetComponent<Order>().information;
        Debug.Log(orderMenu.selectedOrder.GetComponent<Order>().information);
    }

    public void getOrder()
    {
        buttonSound.Play();
        orderMenu.addOrderToQueue();
        orderMenu.selectedOrder = null;
    }

    public void openEmployeeHireMenu()
    {
        commissionMenu.canOpenOrderMenu = false;
        buttonSound.Play();
        int currentIndex = EventSystem.current.currentSelectedGameObject.GetComponent<EmployeeIndex>().index;
        hireEmployee.employeeIndex = currentIndex;
        checkIfEmployeeIsHired.changeButtonsInteractibility(currentIndex);
        foreach (var employee in employees)
        {
            if (currentIndex == employee.GetComponent<EmployeeIndex>().index)
            {
                int index = 0;
                foreach (var employeeInfo in employee.GetComponent<LoadEmployee>().employeeInfoList)
                {
                    employeeInfoList[index++].text = employeeInfo;
                }
            }
        }

        employeeManager.AddEmployee(int.Parse(employeeInfoList[0].text),
                                    employeeInfoList[1].text,
                                    employeeInfoList[2].text,
                                    int.Parse(employeeInfoList[3].text),
                                    employeeInfoList[4].text,
                                    double.Parse(employeeInfoList[5].text));

        hireMenu.enabled = true;
    }

    public void closeEmployeeHireMenu()
    {
        commissionMenu.canOpenOrderMenu = true;
        buttonSound.Play();
        hireMenu.enabled = false;
    }
}
