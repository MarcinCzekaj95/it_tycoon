﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInputs : MonoBehaviour
{
    public Canvas menuCanvas;
    public Inputs keyInput;
    public Canvas gameWindow;

    // Update is called once per frame
    void Update()
    {
        keyDown();
    }

    private void keyDown()
    {
        if (keyInput.escapeKeyDown())
        {
            menuCanvas.enabled = !menuCanvas.enabled;
            gameWindow.enabled = !menuCanvas.enabled;
        }
    }
}
