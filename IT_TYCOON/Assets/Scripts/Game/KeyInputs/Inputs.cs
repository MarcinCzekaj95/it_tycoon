﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inputs : MonoBehaviour
{
    public bool escapeKeyDown()
    {
        return Input.GetKeyDown(KeyCode.Escape);
    }
}
