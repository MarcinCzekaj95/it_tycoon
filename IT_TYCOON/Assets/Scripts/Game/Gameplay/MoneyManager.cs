﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoneyManager : MonoBehaviour
{
    public MoneyUpdate moneyUpdate;
    public double currentMoneyAmount;
    public double difference; // only for debug
    // Start is called before the first frame update
    void Start()
    {
        currentMoneyAmount = DBManager.virtualWallet;
    }

    // Update is called once per frame
    void Update()
    {
        difference = currentMoneyAmount - DBManager.virtualWallet;
        if (currentMoneyAmount != DBManager.virtualWallet)
        {
            moneyUpdate.callMoneyUpdate(currentMoneyAmount - DBManager.virtualWallet);
            currentMoneyAmount = DBManager.virtualWallet;
        }
    }
}
