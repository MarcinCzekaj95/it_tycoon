﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloseMenu : MonoBehaviour
{
    public Canvas menuToClose;
    public OrdersMenu orderMenu;
    public AudioSource buttonSound;

    public void closeMenu()
    {
        buttonSound.Play();
        orderMenu.selectedOrder = null;
        menuToClose.enabled = false;
    }
}
