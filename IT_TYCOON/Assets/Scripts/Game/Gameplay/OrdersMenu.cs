﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class OrdersMenu : MonoBehaviour
{
    public List<GameObject> ordersQueue;
    public EmployeeManager employeeManager;
    public int amountOfOrders;
    public GameObject selectedOrder;
    public Canvas orderInfo;
    public Company company;
    public string orderInformation;
    public Text currentOrderInformation;
    private int devTimer;
    public int companyTimer;
    public AudioSource finishOrderSound;

    private void Start()
    {
        ordersQueue = new List<GameObject>();
    }

    // Update is called once per frame
    void Update()
    {
        var ordersQueueSize = ordersQueue.Count;
        amountOfOrders = ordersQueueSize;

        if (ordersQueueSize > 0)
        {
            setTimer();
            int orderLevel = ordersQueue[0].GetComponent<Order>().orderLevel;
            float timer = ordersQueue[0].GetComponent<Order>().timer;
            int dot = 1;
            switch (ordersQueue[0].GetComponent<Order>().orderType)
            {
                case OrderType.DESKTOP_APP:
                    dot = getProductOfAllEmployees(EmployeeSkill.DESKTOPDEV);
                    currentOrderInformation.text = "Order Type: DESKTOP APP\nTime: " + timer.ToString();
                    AddTextAboutValueAccordingToOrderLevel(orderLevel);
                    break;
                case OrderType.GAME_DEV:
                    dot = getProductOfAllEmployees(EmployeeSkill.GAMEDEV);
                    currentOrderInformation.text = "Order Type: Game Dev\nTime: " + timer.ToString();
                    AddTextAboutValueAccordingToOrderLevel(orderLevel);
                    break;
                case OrderType.WEB_APP:
                    dot = getProductOfAllEmployees(EmployeeSkill.WEBDEV);
                    currentOrderInformation.text = "Order Type: WEB APP\nTime: " + timer.ToString();
                    AddTextAboutValueAccordingToOrderLevel(orderLevel);
                    break;
                default:
                    Debug.Log("ERROR: Cannot find order");
                    break;
            }
            ordersQueue[0].GetComponent<Order>().timer -= ((dot+ companyTimer) * Time.deltaTime);
            if (ordersQueue[0].GetComponent<Order>().timer <= 0.0f)
            {
                finishOrderSound.Play();
                if (ordersQueue[0].GetComponent<Order>().orderLevel == 1)
                {
                    double value = company.level * 500;
                    DBManager.virtualWallet += value;
                    company.value += value;
                }
                if (ordersQueue[0].GetComponent<Order>().orderLevel == 2)
                {
                    double value = company.level * 2000;
                    DBManager.virtualWallet += value;
                    company.value += value;
                }
                if (ordersQueue[0].GetComponent<Order>().orderLevel == 3)
                {
                    double value = company.level * 4000;
                    DBManager.virtualWallet += value;
                    company.value += value;
                }
                var objToDestroy = ordersQueue[0];
                ordersQueue.Remove(objToDestroy);
                Destroy(objToDestroy);
                currentOrderInformation.text = "";
            }
        }
    }

    private void setTimer()
    {
        var newOrder = ordersQueue[0].GetComponent<Order>();
        if (!newOrder.isStarted)
        {
            Debug.Log("timer set");
            if (newOrder.orderLevel == 1)
            {
                newOrder.isStarted = true;
                newOrder.timer = Random.Range(20.0f, 50.0f);
            }
            else if (newOrder.orderLevel == 2)
            {
                newOrder.isStarted = true;
                newOrder.timer = Random.Range(100.0f, 150.0f);
            }
            else if (newOrder.orderLevel == 3)
            {
                newOrder.isStarted = true;
                newOrder.timer = Random.Range(200.0f, 300.0f);
            }
        }
    }

    public void addOrderToQueue()
    {
        if (selectedOrder != null)
        {
            Debug.Log(selectedOrder.gameObject.name);
            
            ordersQueue.Add(selectedOrder);
            selectedOrder.GetComponent<Button>().enabled = false;
            orderInfo.enabled = false;

        }
        else
        {
            Debug.Log("You should select order");
        }
    }

    private void AddTextAboutValueAccordingToOrderLevel(int orderLevel)
    {
        double value = 0.0;
        switch (orderLevel)
        {
            case 1:
                value = 500.0 * company.level;
                break;
            case 2:
                value = 2000.0 * company.level;
                break;
            case 3:
                value = 4000.0 * company.level;
                break;
            default:
                Debug.Log("Level does not exist");
                break;
        }
        currentOrderInformation.text += "\nValue:" + value.ToString() + "$";
    }

    private int getProductOfAllEmployees(EmployeeSkill employeeSkill)
    {
        var allDesktopDevs = employeeManager.employeesList.FindAll(x => x.employeeSkill == employeeSkill);
        int result = 1;
        foreach (var dektopDev in allDesktopDevs)
        {
            if (dektopDev.value == 20000)
            {
                result += 5;
            }
            else if (dektopDev.value == 10000)
            {
                result += 3;
            }
            else
            {
                result += 2;
            }
        }
        return result;
    }
}
