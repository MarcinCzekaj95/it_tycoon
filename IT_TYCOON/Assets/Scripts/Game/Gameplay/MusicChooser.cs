﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicChooser : MonoBehaviour
{
    public AudioSource backgroundMusic;
    // Start is called before the first frame update
    void Start()
    {
        if (DBManager.comapnyName == "Death Stranding")
        {
            backgroundMusic.clip = Resources.Load<AudioClip>("bones");
            backgroundMusic.Play();
        }
    }
}
