﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UpdateCompany : MonoBehaviour
{
    public Company company;
    public MoneyManager moneyManager;
    public MoneyUpdate moneyUpdate;
    public Text errorInformation;
    public float timer = 0.5f;

    void Update()
    {
        if (errorInformation.text != "")
        {
            timer -= 0.2f * Time.deltaTime;
            if (timer <= 0.0f)
            {
                timer = 0.5f;
                errorInformation.text = "";
            }
        }
    }

    public void callCompanyValueUpdate(double difference)
    {
        StartCoroutine(comapnyValueUpdate(difference));
    }

    IEnumerator comapnyValueUpdate(double difference)
    {
        WWWForm form = new WWWForm();
        form.AddField("CompanyName", DBManager.comapnyName);
        form.AddField("Difference", difference.ToString());
        WWW www = new WWW("http://localhost:8080/sqlconnect/UpdateCompanyValue.php", form);
        yield return www;
        if (www.text == "0")
        {
            Debug.Log("Actual company value updated");
        }
        else
        {
            errorInformation.text = "Failed to add money to your account. ERROR #" + www.text;
            Debug.Log("Failed to add money to your account. ERROR #" + www.text);
        }
    }

    public bool callCompanyLevel()
    {
        if (company.level == 1)
        {
            if (company.value >= 100000 && DBManager.virtualWallet >= 100000)
            {
                double valueToAdd = 100000;
                StartCoroutine(companyLevelUpdate(2, valueToAdd));
                return true;
            }
        }
        if (company.level == 2)
        {
            if (company.value >= 500000 && DBManager.virtualWallet >= 500000)
            {
                double valueToAdd = 500000;
                StartCoroutine(companyLevelUpdate(3, valueToAdd));
                return true;
            }
        }
        if (company.level == 3)
        {
            if(DBManager.virtualWallet >= 1000000)
            {
                double valueToAdd = 200000;
                StartCoroutine(companyLevelUpdate(4, valueToAdd));
                return true;
            }
        }
        return false;
    }

    IEnumerator companyLevelUpdate(int level, double valueToAdd)
    {
        WWWForm form = new WWWForm();
        form.AddField("CompanyName", DBManager.comapnyName);
        if(level == 4)
        {
            int currentLevel = 3;
            form.AddField("CompanyLevel", currentLevel.ToString());
        }
        else
        {
            form.AddField("CompanyLevel", level.ToString());
        }

        WWW www = new WWW("http://localhost:8080/sqlconnect/UdpateCompanyLevel.php", form);
        yield return www;
        if (www.text == "0")
        {
            if (level == 4)
            {
                DBManager.virtualWallet -= 1000000;
            }
            else
            {
                DBManager.virtualWallet -= valueToAdd;
            }
            Debug.Log("Upragde Company Successfuly");
            moneyUpdate.callMoneyUpdate(moneyManager.currentMoneyAmount - DBManager.virtualWallet);
            if (level < 4)
            {
                SceneManager.LoadScene(level);
            }
        }
        else
        {
            errorInformation.text = "Failed to update compnay level. ERROR #" + www.text;
            Debug.Log("Failed to update compnay level. ERROR #" + www.text);
        }
    }
}
