﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadCompany : MonoBehaviour
{
    public GameObject companyObject;
    public CompanyManager companyManager;

    private void Start()
    {
        companyObject.GetComponent<Company>().name = DBManager.comapnyName;
        callLoadCompany();
    }

    public void callLoadCompany()
    {
        StartCoroutine(loadCompany());
    }

    IEnumerator loadCompany()
    {
        WWWForm form = new WWWForm();
        form.AddField("CompanyName", DBManager.comapnyName);
        WWW www = new WWW("http://localhost:8080/sqlconnect/LoadCompany.php", form);
        yield return www;
        if (www.text[0] == '0')
        {
            string[] companyInfoStrings = www.text.Split('\t');
            double companyValue = double.Parse(companyInfoStrings[1]);
            int companyLevel = int.Parse(companyInfoStrings[2]);
            companyObject.GetComponent<Company>().value = companyValue;
            companyObject.GetComponent<Company>().level = companyLevel;
            companyManager.currentLevel = companyLevel;
            companyManager.currentVaule = companyValue;
            DBManager.companyValue = companyValue;
        }
        else
        {
            Debug.Log("Failed load company informations. ERROR #" + www.text);
        }
    }
}
