﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayPlayeInformation : MonoBehaviour
{
    public Text realMoneyValue;
    public Text companyValue;
    public CompanyManager companyManager;
    public double currentMoney;
    public double currentCompanyValue;

    void Start()
    {
        realWalletUpdate();
        companyValueUpdate();
    }

    void Update()
    {
        if(DBManager.realWallet != currentMoney)
        {
            realWalletUpdate();
        }
        if(companyManager.company.value != currentCompanyValue)
        {
            companyValueUpdate();
        }
    }

    private void realWalletUpdate()
    {
        currentMoney = DBManager.realWallet;
        realMoneyValue.text = "Your Wallet: " + DBManager.realWallet.ToString() + "$";
    }

    private void companyValueUpdate()
    {
        currentCompanyValue = companyManager.company.value;
        companyValue.text = "Company Value: " + companyManager.company.value.ToString() + "$";
    }
}
