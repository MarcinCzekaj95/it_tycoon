﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadHiredEmployees : MonoBehaviour
{
    public EmployeeManager employeeManager;
    public int currentAmountOfHiredEmployees;
    public List<Text> hiredEmployees;

    // Start is called before the first frame update
    void Start()
    {
        if(employeeManager.employeesList.Count > 0)
        {
            currentAmountOfHiredEmployees = employeeManager.employeesList.Count;
            int i = 0;
            foreach (var employee in employeeManager.employeesList)
            {
                hiredEmployees[i++].text = "Name: " + employee.name + " Value: " + employee.value.ToString() + " Skill: " + employee.skill;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (employeeManager.employeesList.Count != currentAmountOfHiredEmployees)
        {
            Debug.Log("UPDATE HIRED EMPLOYEE LIST");
            currentAmountOfHiredEmployees = employeeManager.employeesList.Count;
            for(int index = 0; index < hiredEmployees.Count; ++index)
            {
                hiredEmployees[index].text = "";
            }
            int i = 0;
            foreach (var employee in employeeManager.employeesList)
            {
                hiredEmployees[i++].text = "Name: " + employee.name + " Value: " + employee.value.ToString() + " Skill: " + employee.skill;
            }
        }
    }
}
