﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadCompanyEmployees : MonoBehaviour
{
    public EmployeeManager employeeManager;

    // Start is called before the first frame update
    void Start()
    {
        for(int id = 1; id <= 9; ++id)
        {
            loadComapnyEmployees(id);
        }
    }

    public void loadComapnyEmployees(int id)
    {
        StartCoroutine(loadEmployees(id));
    }

    IEnumerator loadEmployees(int id)
    {
        WWWForm form = new WWWForm();
        form.AddField("EmployeeId", id);
        form.AddField("CompanyName", DBManager.comapnyName);
        WWW www = new WWW("http://localhost:8080/sqlconnect/LoadHiredEmployees.php", form);
        yield return www;
        if (www.text[0] == '0')
        {
            Debug.Log("Udalo sie");
            string[] employeeInfo = www.text.Split('\t');
            employeeManager.AddEmployee(int.Parse(employeeInfo[1]),
                                       employeeInfo[2],
                                       employeeInfo[3],
                                       int.Parse(employeeInfo[4]),
                                       employeeInfo[5],
                                       double.Parse(employeeInfo[6]));
            int employeeIndex = id - 1;
            var employee = GameObject.Find("Employee" + employeeIndex);
            employee.GetComponent<Employee>().SetEmployeeValues(employeeManager.employee);
            employeeManager.employeesList.Add(employee.GetComponent<Employee>());
        }
        else
        {
            Debug.Log("Failed to Add Employee" + www.text);
        }
    }
}
