﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadEmployee : MonoBehaviour
{
    public List<string> employeeInfoList;

    private void Start()
    {
        callLoadEmployee();
    }

    public void callLoadEmployee()
    {
        StartCoroutine(loadEmployee());
    }

    IEnumerator loadEmployee()
    {
        int employeeIndex = GetComponent<EmployeeIndex>().index;
        WWWForm form = new WWWForm();
        form.AddField("Id", employeeIndex);
        WWW www = new WWW("http://localhost:8080/sqlconnect/LoadEmployee.php", form);
        yield return www;
        if (www.text[0] == '0')
        {
            string[] employeeInfoStrings = www.text.Split('\t');
            int index = 0;
            foreach(string info in employeeInfoStrings)
            {
                if (info != "0")
                {
                    employeeInfoList[index++] = info;
                }
            }
        }
        else
        {
            Debug.Log("Failed to add money to your account. ERROR #" + www.text);
        }
    }
}
