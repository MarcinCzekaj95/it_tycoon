﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CompanyManager : MonoBehaviour
{
    public Company company;
    public double currentVaule;
    public int currentLevel;
    public LoadCompany loadCompany;
    public UpdateCompany updateCompany;

    void Start()
    {
        company.name = DBManager.comapnyName;
    }

    void Update()
    {
        if((company.value != currentVaule)
           || (company.level != currentLevel))
        {
            Debug.Log("Update Company Value");
            double diff = company.value - currentVaule;

            Debug.Log(diff);

            updateCompany.callCompanyValueUpdate(diff);
            loadCompany.callLoadCompany();
            currentVaule = company.value;
        }
    }

}
