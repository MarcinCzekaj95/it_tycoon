﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EmployeeSkill
{
    GAMEDEV = 0,
    WEBDEV = 1,
    DESKTOPDEV = 2
}

public class Employee : MonoBehaviour
{
    public int id;
    public string name;
    public string surname;
    public int age;
    public string skill;
    public double value;
    public EmployeeSkill employeeSkill;

    public Employee(int id, string name, string surname, int age, string skill, double value)
    {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.skill = skill;
        this.value = value;
        switch(skill)
        {
            case "GameDev":
                employeeSkill = EmployeeSkill.GAMEDEV;
                break;
            case "DesktopDev":
                employeeSkill = EmployeeSkill.DESKTOPDEV;
                break;
            case "WebDev":
                employeeSkill = EmployeeSkill.WEBDEV;
                break;
            default:
                Debug.Log("ERROR: Cannot find skill");
                break;
        }
    }

    public Employee(Employee employee)
    {
        this.id = employee.id;
        this.name = employee.name;
        this.surname = employee.surname;
        this.age = employee.age;
        this.skill = employee.skill;
        this.value = employee.value;
        this.employeeSkill = employee.employeeSkill;
    }

    public void SetEmployeeValues(int id, string name, string surname, int age, string skill, double value)
    {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.skill = skill;
        this.value = value;
        switch (this.skill)
        {
            case "GameDev":
                employeeSkill = EmployeeSkill.GAMEDEV;
                break;
            case "DesktopDev":
                employeeSkill = EmployeeSkill.DESKTOPDEV;
                break;
            case "WebDev":
                employeeSkill = EmployeeSkill.WEBDEV;
                break;
            default:
                Debug.Log("ERROR: Cannot find skill");
                break;
        }
    }

    public void SetEmployeeValues(Employee employee)
    {
        this.id = employee.id;
        this.name = employee.name;
        this.surname = employee.surname;
        this.age = employee.age;
        this.skill = employee.skill;
        this.value = employee.value;
        switch (this.skill)
        {
            case "GameDev":
                employeeSkill = EmployeeSkill.GAMEDEV;
                break;
            case "DesktopDev":
                employeeSkill = EmployeeSkill.DESKTOPDEV;
                break;
            case "WebDev":
                employeeSkill = EmployeeSkill.WEBDEV;
                break;
            default:
                Debug.Log("ERROR: Cannot find skill");
                break;
        }
    }
}
