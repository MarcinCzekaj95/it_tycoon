﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Company : MonoBehaviour
{
    public int level;
    public double value;
    public string name;

    public Company(Company company)
    {
        this.level = company.level;
        this.value = company.value;
        this.name = company.name;
    }

    public void SetEmployeeValues(int level, double value, string name)
    {
        this.level = level;
        this.value = value;
        this.name = name;
    }
}
