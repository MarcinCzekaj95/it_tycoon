﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pointers : MonoBehaviour
{
    public GameObject arrow;
    public Transform toPosition;
    public Transform fromPosition;
    private Vector3 directPosition;
    private const float SPEED = 1.5f;

    private void Start()
    {
        directPosition = toPosition.position;
    }

    void Update()
    {
        float step = SPEED * Time.deltaTime;
        if(arrow.transform.position.Equals(toPosition.position))
        {
            directPosition = fromPosition.position;
        }
        else if(arrow.transform.position.Equals(fromPosition.position))
        {
            directPosition = toPosition.position;
        }
        arrow.transform.position = Vector3.MoveTowards(arrow.transform.position, directPosition, step);
    }
}
