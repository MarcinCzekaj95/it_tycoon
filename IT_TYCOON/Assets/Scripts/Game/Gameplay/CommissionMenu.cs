﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CommissionMenu : MonoBehaviour
{
    public Canvas commisionMenuCanvas;
    public bool canOpenOrderMenu = true;

    private void OnMouseDown()
    {
        if (canOpenOrderMenu)
        {
            commisionMenuCanvas.enabled = true;
        }
    }
}
