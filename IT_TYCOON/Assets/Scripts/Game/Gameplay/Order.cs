﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum OrderType
{
    GAME_DEV = 0,
    DESKTOP_APP = 1,
    WEB_APP = 2
}


public class Order : MonoBehaviour
{
    public int orderLevel;
    public OrderType orderType;
    public float timer;
    public bool isStarted = false;
    public static int index = 0;
    public string information;
    public Company company;

    // Start is called before the first frame update
    void Start()
    {
        company = GameObject.Find("CompanyInformation").GetComponent<Company>();
        index++;
        if (index > 20)
        {
            index = 0;
        }
        orderLevel = Random.Range(1, 4);
        addInformationAboutProduct();
    }

    private void addInformationAboutProduct()
    {
        switch (orderType)
        {
            case OrderType.GAME_DEV:
                information = "Create Openworld game \nPlatforms PS4, Xbox One, PC";
                AddTextAboutValueAccordingToOrderLevel(orderLevel);
                break;
            case OrderType.DESKTOP_APP:
                information = "Create new system for distributing files Desktop Application";
                AddTextAboutValueAccordingToOrderLevel(orderLevel);
                break;
            case OrderType.WEB_APP:
                information = "Create Web page for managing rankings in games";
                AddTextAboutValueAccordingToOrderLevel(orderLevel);
                break;
            default:
                Debug.Log("Error object does not exist");
                break;
        }
    }

    private void AddTextAboutValueAccordingToOrderLevel(int orderLevel)
    {
        double value = 0.0;
        switch (orderLevel)
        {
            case 1:
                value = 500.0 * company.level;
                break;
            case 2:
                value = 2000.0 * company.level;
                break;
            case 3:
                value = 4000.0 * company.level;
                break;
            default:
                Debug.Log("Level does not exist");
                break;
        }
        information += "\nValue:" + value.ToString() + "$";
    }
}
