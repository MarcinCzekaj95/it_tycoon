﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CheckIfEmployeedIsHired : MonoBehaviour
{
    public EmployeeManager employeeManager;
    public Button hireButton;
    public Button fireButton;

    public void changeButtonsInteractibility(int employeeIndex)
    {
        bool employeeHired = employeeManager.employeesList.Exists(x => x.id == employeeIndex);

        if(employeeHired)
        {
            hireButton.interactable = false;
            fireButton.interactable = true;
        }
        else
        {
            hireButton.interactable = true;
            fireButton.interactable = false;
        }
    }
}
