﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EmployeeManager : MonoBehaviour
{
    public Employee employee;
    public List<Employee> employeesList;

    public float timer;

    private void Start()
    {
        timer = 3000.0f;
    }

    private void Update()
    {
        if (timer <= 0.0f)
        {
            foreach (var employee in employeesList)
            {
                DBManager.virtualWallet -= employee.value;
            }
            timer = 3000.0f;
        }
        timer -= Time.deltaTime;
    }

    public void AddEmployee(int id, string name, string surname, int age, string skill, double value)
    {
        employee.id = id;
        employee.name = name;
        employee.surname = surname;
        employee.age = age;
        employee.skill = skill;
        employee.value = value;
        switch (skill)
        {
            case "GameDev":
                employee.employeeSkill = EmployeeSkill.GAMEDEV;
                break;
            case "DesktopDev":
                employee.employeeSkill = EmployeeSkill.DESKTOPDEV;
                break;
            case "WebDev":
                employee.employeeSkill = EmployeeSkill.WEBDEV;
                break;
            default:
                Debug.Log("ERROR: Cannot find skill");
                break;
        }
    }
}
