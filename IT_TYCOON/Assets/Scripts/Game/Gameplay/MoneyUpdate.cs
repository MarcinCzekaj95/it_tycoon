﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoneyUpdate : MonoBehaviour
{
    public void callMoneyUpdate(double difference)
    {
        StartCoroutine(moneyUpdate(difference));
    }

    IEnumerator moneyUpdate(double difference)
    {
        WWWForm form = new WWWForm();
        form.AddField("UserName", DBManager.userName);
        form.AddField("Difference", difference.ToString());
        WWW www = new WWW("http://localhost:8080/sqlconnect/UpdateMoney.php", form);
        yield return www;
        if (www.text[0] == '0')
        {
            Debug.Log("Money Update successful actual state = " + DBManager.virtualWallet);
            DBManager.realWallet = double.Parse(www.text.Split('\t')[1]);
        }
        else
        {
            Debug.Log("Failed to add money to your account. ERROR #" + www.text);
        }
    }
}
