﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class AddOrders : MonoBehaviour
{
    public DisplayMenus ordersMenu;
    public List<GameObject> orderList;
    public int tagIndex = 0;
    [SerializeField]
    private float timer;
    public int companyLevel;
    public AudioSource newOrderSound;

    // Start is called before the first frame update
    void Start()
    {
        ordersMenu = GameObject.FindWithTag("MenuManager").GetComponent<DisplayMenus>();
        timer = Random.Range(30.0f, 60.0f);
    }

    // Update is called once per frame
    void Update()
    {
        newOrderRequest();
    }

    private void newOrderRequest()
    {
        if (timer <= 0)
        {
            newOrderSound.Play();
            GameObject newObj;
            int index = Random.Range(0, 3);
            newObj = (GameObject)Instantiate(orderList[index], transform);

            newObj.name = "Order" + Order.index.ToString();

            newObj.GetComponent<Button>().onClick.AddListener(ordersMenu.openOrderInfoMenu);

            timer = Random.Range(30.0f, 60.0f);
        }
        timer -= (companyLevel * Time.deltaTime);
    }
}
