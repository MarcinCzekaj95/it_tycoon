﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestScript : MonoBehaviour
{
    // Start is called before the first frame update
    IEnumerator Start()
    {
        WWW request = new WWW("http://localhost:8080/sqlconnect/webtest.php");
        yield return request;
        string[] stringArray = request.text.Split('\t');
        foreach(var text in stringArray)
        {
            Debug.Log(text);
        }
    }
}
