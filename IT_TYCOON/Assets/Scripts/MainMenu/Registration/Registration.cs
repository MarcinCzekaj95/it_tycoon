﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Registration : MonoBehaviour
{
    public InputField m_login;
    public InputField m_password;
    public Button m_submitButton;

    public AudioSource buttonSound;

    public void callRegister()
    {
        buttonSound.Play();
        StartCoroutine(register());
    }

    IEnumerator register()
    {
        WWWForm l_form = new WWWForm();
        l_form.AddField("name", m_login.text);
        l_form.AddField("password", m_password.text);
        WWW l_www = new WWW("http://localhost:8080/sqlconnect/register.php", l_form);
        yield return l_www;
        if(l_www.text == "0")
        {
            Debug.Log("User created successfuly");
            UnityEngine.SceneManagement.SceneManager.LoadScene(0);
        }
        else
        {
            Debug.Log("User creation failed. ERROR # " + l_www.text);
        }
    }

    public void verifyInput()
    {
        m_submitButton.interactable = (m_login.text.Length >= 8 && m_password.text.Length >= 8);
    }

}
