﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Options : MonoBehaviour
{
    public AudioSource buttonSound;

    public void onClickHelp()
    {
        buttonSound.Play();
        Debug.Log("Help Clicked");
    }

    public void onClickBack()
    {
        buttonSound.Play();
        GetComponent<MenuManager>().changeMenu();
    }
}
