﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Store : MonoBehaviour
{
    public AudioSource buttonSound;

    public void onClickBuy50k()
    {
        buttonSound.Play();
        Debug.Log("YOU BOUGHT 50.000");
    }

    public void onClickBuy100k()
    {
        buttonSound.Play();
        Debug.Log("YOU BOUGHT 100.000");
    }

    public void onClickBuy200k()
    {
        buttonSound.Play();
        Debug.Log("YOU BOUGHT 200.000");
    }

    public void onClickBack()
    {
        buttonSound.Play();
        GetComponent<MenuManager>().changeMenu();
    }
}
