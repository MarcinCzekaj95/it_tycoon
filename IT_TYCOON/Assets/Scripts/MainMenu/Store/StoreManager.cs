﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class StoreManager : MonoBehaviour
{
    public Text playerDisplayInformation;
    public AudioSource buttonSound;
    public Text errorInformation;
    public float timer = 0.5f;
    private double maxRealMoneyToPay = 0.0;
    private double amountOfMoney = 0.0;
    public int index;

    void Update()
    {
        if (errorInformation.text != "")
        {
            timer -= 0.2f * Time.deltaTime;
            if (timer <= 0.0f)
            {
                timer = 0.5f;
                errorInformation.text = "";
            }
        }
    }

    public void callBuyMoney()
    {
        index = EventSystem.current.currentSelectedGameObject.GetComponent<MoneyLevelToBuy>().index;
        buttonSound.Play();

        switch (index)
        {
            case 1:
                amountOfMoney = 5000.0;
                maxRealMoneyToPay = 2.0;
                break;
            case 2:
                amountOfMoney = 10000.0;
                maxRealMoneyToPay = 4.0;
                break;
            case 3:
                amountOfMoney = 20000.0;
                maxRealMoneyToPay = 8.0;
                break;
            default:
                amountOfMoney = 0.0;
                maxRealMoneyToPay = 0.0;
                break;
        }
        if (DBManager.realWallet >= maxRealMoneyToPay)
        {
            StartCoroutine(buyMoney());
        }
        else
        {
            errorInformation.text = "Cannot buy virtual money, because you don't have enough real money";
            Debug.Log("Cannot buy virtual money, because you don't have enough money in your wallet");
        }
    }

    IEnumerator buyMoney()
    {
        WWWForm l_form = new WWWForm();
        l_form.AddField("name", DBManager.userName);
        l_form.AddField("MoneyToAdd", amountOfMoney.ToString());
        l_form.AddField("RealMoneyToSubstract", maxRealMoneyToPay.ToString());
        WWW l_www = new WWW("http://localhost:8080/sqlconnect/buyMoney.php", l_form);

        yield return l_www;
        if (l_www.text[0] == '0')
        {
            double realMoney = double.Parse(l_www.text.Split('\t')[1], CultureInfo.InvariantCulture);
            DBManager.virtualWallet += amountOfMoney;
            DBManager.realWallet -= maxRealMoneyToPay;
            playerDisplayInformation.text = "Player: " + DBManager.userName + " Money: " + DBManager.virtualWallet;
            errorInformation.text = "Congratulations you've got money: Amount = " + amountOfMoney;
        }
        else
        {
            errorInformation.text = "Failed to add money to your account. ERROR #" + l_www.text;
            Debug.Log("Failed to add money to your account. ERROR #" + l_www.text);
        }

    }
}
