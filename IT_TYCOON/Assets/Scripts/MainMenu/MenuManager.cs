﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuManager : MonoBehaviour
{
    public GameObject currentMenu;
    public GameObject nextMenu;
    public AudioSource buttonSound;

    public void changeMenu()
    {
        buttonSound.Play();
        currentMenu.GetComponent<Canvas>().enabled = false;
        nextMenu.GetComponent<Canvas>().enabled = true;
    }
}
