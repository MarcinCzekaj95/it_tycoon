﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    public MenuManager[] menuManagers;
    public AudioSource buttonSound;

    public void onClickStart()
    {
        buttonSound.Play();
        menuManagers[2].changeMenu();
    }

    public void onClickExit()
    {
        buttonSound.Play();
        Application.Quit();
    }

    public void onClickBackToMainMenu()
    {
        buttonSound.Play();
        SceneManager.LoadScene(0);
    }

    public void onClickOptions()
    {
        buttonSound.Play();
        menuManagers[0].changeMenu();
        Debug.Log("OPTIONS Clicked");
    }

    public void onClickStore()
    {
        buttonSound.Play();
        menuManagers[1].changeMenu();
        Debug.Log("STORE Clicked");
    }
}
