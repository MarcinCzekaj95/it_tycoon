﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class OpenCompanyCreator : MonoBehaviour
{
    public MenuManager menuManager;

    public void callCheckIfCompanyExist()
    {
        StartCoroutine(checkIfCompanyExist());
    }

    IEnumerator checkIfCompanyExist()
    {
        WWWForm form = new WWWForm();
        form.AddField("UserName", DBManager.userName);
        WWW www = new WWW("http://localhost:8080/sqlconnect/checkIfplayerHasCompany.php", form);
        yield return www;
        if (www.text[0] == '0')
        {
            string[] stringArray = www.text.Split('\t');
            DBManager.comapnyName = stringArray[1];
            int companyScene = int.Parse(stringArray[2]);
            SceneManager.LoadScene(companyScene);
        }
        else
        {
            Debug.Log("Error cannot find company: " + www.text);
            if(www.text[0] == '7')
            {
                menuManager.changeMenu();
            }
        }

    }
}
