﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoginPlayer : MonoBehaviour
{
    public Text playerDisplayInformation;
    public Text errorInformation;

    public InputField m_login;
    public InputField m_password;
    public Button m_submitButton;

    public AudioSource buttonSound;

    public void callLogin()
    {
        StartCoroutine(login());
    }

    IEnumerator login()
    {
        WWWForm l_form = new WWWForm();
        l_form.AddField("name", m_login.text);
        l_form.AddField("password", m_password.text);
        WWW l_www = new WWW("http://localhost:8080/sqlconnect/login.php", l_form);
        yield return l_www;
        if (l_www.text[0] == '0')
        {
            DBManager.userName = m_login.text;
            DBManager.virtualWallet = double.Parse(l_www.text.Split('\t')[1]);
            DBManager.realWallet = double.Parse(l_www.text.Split('\t')[2]);
            GetComponentInChildren<MenuManager>().changeMenu();
            playerDisplayInformation.text = "Player: " + DBManager.userName + " Money: " + DBManager.virtualWallet;
        }
        else
        {
            errorInformation.text = "User log-in failed. ERROR #" + l_www.text;
        }

    }

    public void verifyInput()
    {
        m_submitButton.interactable = (m_login.text.Length >= 8 && m_password.text.Length >= 8);
    }
}
