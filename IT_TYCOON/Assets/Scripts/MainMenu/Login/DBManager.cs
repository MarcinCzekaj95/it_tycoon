﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class DBManager
{
    public static string userName;
    public static double virtualWallet;
    public static string comapnyName;
    public static double realWallet;
    public static double companyValue;

    public static bool logedIn
    {
        get { return userName != null; }
    }

    public static void logOut()
    {
        userName = null;
    }
}
