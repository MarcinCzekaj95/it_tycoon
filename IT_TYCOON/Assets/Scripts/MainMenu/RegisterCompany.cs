﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class RegisterCompany : MonoBehaviour
{
    public Text errorInformation;
    public InputField m_companyName;
    public Button m_submitButton;
    public AudioSource buttonSound;

    public void callRegisterCompany()
    {
        buttonSound.Play();
        StartCoroutine(registerCompany());
    }

    IEnumerator registerCompany()
    {
        WWWForm form = new WWWForm();
        form.AddField("UserName", DBManager.userName);
        form.AddField("CompanyName", m_companyName.text);
        WWW www = new WWW("http://localhost:8080/sqlconnect/registerCompany.php", form);
        yield return www;
        if (www.text[0] == '0')
        {
            DBManager.comapnyName = m_companyName.text;
            SceneManager.LoadScene(1);
        }
        else
        {
            errorInformation.text = "Create new company failed. ERROR #" + www.text;
        }

    }

    public void verifyInput()
    {
        m_submitButton.interactable = (m_companyName.text.Length >= 8);
    }
}
