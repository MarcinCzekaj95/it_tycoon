-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 17, 2019 at 08:57 PM
-- Server version: 5.7.24
-- PHP Version: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `it_tycoon`
--

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE `companies` (
  `Id` int(11) NOT NULL,
  `CompanyName` varchar(100) NOT NULL,
  `Value` int(11) NOT NULL,
  `Level` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`Id`, `CompanyName`, `Value`, `Level`) VALUES
(5, 'HugeGames', 10000, 1);

-- --------------------------------------------------------

--
-- Table structure for table `companytoplayer`
--

CREATE TABLE `companytoplayer` (
  `Id` int(11) NOT NULL,
  `PlayerId` int(11) NOT NULL,
  `CompanyId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `companytoplayer`
--

INSERT INTO `companytoplayer` (`Id`, `PlayerId`, `CompanyId`) VALUES
(4, 4, 5);

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `Id` int(11) NOT NULL,
  `Name` varchar(300) NOT NULL,
  `Surname` varchar(300) NOT NULL,
  `Age` int(11) NOT NULL,
  `Skill` varchar(300) NOT NULL,
  `Value` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`Id`, `Name`, `Surname`, `Age`, `Skill`, `Value`) VALUES
(1, 'Marcin', 'Czekaj', 24, 'GameDev', 5000),
(2, 'Wojtek', 'Urban', 28, 'WebDev', 5000),
(3, 'Andrzej', 'Kucyk', 28, 'DesktopDev', 5000),
(4, 'Krzysiek', 'Kaniak', 35, 'GameDev', 10000),
(5, 'Michal', 'Mistrz', 35, 'DesktopDev', 20000),
(6, 'Andrzej', 'Wazny', 30, 'WebDev', 20000),
(7, 'Piotrek', 'Mizerny', 26, 'DesktopDev', 10000),
(8, 'Piotrek', 'Nowak', 30, 'GameDev', 20000),
(9, 'Dominik', 'Kowalski', 25, 'DesktopDev', 10000);

-- --------------------------------------------------------

--
-- Table structure for table `employeetocompnay`
--

CREATE TABLE `employeetocompnay` (
  `Id` int(11) NOT NULL,
  `employeeId` int(11) NOT NULL,
  `companyId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `players`
--

CREATE TABLE `players` (
  `Id` int(11) NOT NULL,
  `Name` varchar(15) NOT NULL,
  `Hash` varchar(100) NOT NULL,
  `Salt` varchar(50) NOT NULL,
  `Wallet` double NOT NULL DEFAULT '0',
  `VirtualWallet` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `players`
--

INSERT INTO `players` (`Id`, `Name`, `Hash`, `Salt`, `Wallet`, `VirtualWallet`) VALUES
(4, 'MarcinCzekaj', '$5$rounds=5000$unpredictableMar$AKAx88ZxsFbSHVQJnbNCHUvbQR61jl2JAJBlvsaoqg.', '$5$rounds=5000$unpredictableMarcinCzekaj$', 0, 70000);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `companytoplayer`
--
ALTER TABLE `companytoplayer`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `employeetocompnay`
--
ALTER TABLE `employeetocompnay`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `players`
--
ALTER TABLE `players`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `Name` (`Name`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `companies`
--
ALTER TABLE `companies`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `companytoplayer`
--
ALTER TABLE `companytoplayer`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `employeetocompnay`
--
ALTER TABLE `employeetocompnay`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `players`
--
ALTER TABLE `players`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
