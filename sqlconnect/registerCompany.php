<?php
	$con = mysqli_connect('localhost', 'root', 'root', 'IT_TYCOON');
	
	//check that connection happend
	if(mysqli_connect_errno())
	{
		echo "1: Connection failed"; //error code #1 = connection failed
		exit();
	}
	
	$userName = $_POST['UserName'];
	$companyName = $_POST['CompanyName'];

	$playerNameCheckQuery = "SELECT Id FROM players WHERE Name='" . $userName . "';";
	
	$getPlayerId = mysqli_query($con, $playerNameCheckQuery) or die("2: Name check failed"); //error code #2 - name check query failed
	
	if(mysqli_num_rows($getPlayerId) != 1)
	{
		echo "3: Either no user with name, or more than one"; //error code #3 - number names matching does not equal 1
		exit();
	}
	
	$companyCheckQuery = "SELECT Id FROM companies WHERE CompanyName='" . $companyName . "';";
	
	$checkIfCompanyExist = mysqli_query($con, $companyCheckQuery) or die("4: Company Check failed"); //error code #2 - name check query failed
	
	if(mysqli_num_rows($checkIfCompanyExist) > 0)
	{
		echo "5: Company Exist"; //error code #3 - number names matching does not equal 1
		exit();
	}
	
	$insertCompany = "INSERT INTO companies (CompanyName, Value, Level) VALUES('" . $companyName . "', 10000, 1);";
	
	mysqli_query($con, $insertCompany) or die("6: Insert new company failed"); //error code #6 - insert query failed
	
	$getCompanyId = mysqli_query($con, $companyCheckQuery) or die("7: Company Check failed"); //error code #2 - name check query failed
	
	if(mysqli_num_rows($getCompanyId) != 1)
	{
		echo "8: Company does not exists"; //error code #3 - number names matching does not equal 1
		exit();
	}
	
	$playerInfo = mysqli_fetch_assoc($getPlayerId);
	$playerId = $playerInfo["Id"];
	$companyInfo = mysqli_fetch_assoc($getCompanyId);
	$companyId = $companyInfo["Id"];
	
	$attachCompanyToPlayer = "INSERT INTO companytoplayer (PlayerId, CompanyId) VALUES(".$playerId.", ".$companyId.");";
	
	mysqli_query($con, $attachCompanyToPlayer) or die("9: Attach player to comapny failed"); //error code #9 - insert query failed
	
	echo "0";

?>