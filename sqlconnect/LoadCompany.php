<?php
	$con = mysqli_connect('localhost', 'root', 'root', 'IT_TYCOON');
	
	//check that connection happend
	if(mysqli_connect_errno())
	{
		echo "1: Connection failed"; //error code #1 = connection failed
		exit();
	}
	
	$companyName = $_POST['CompanyName'];
	
	$companyExistChecker = "SELECT c.value, c.level FROM companytoplayer as ctp JOIN companies as c WHERE c.CompanyName = '" . $companyName . "' and ctp.CompanyId = c.Id;";
	
	$getCompanyValues = mysqli_query($con, $companyExistChecker) or die("2: Company Check Failed"); //error code #2 - id check query failed
	
	if(mysqli_num_rows($getCompanyValues) != 1)
	{
		echo "3: Either one or none of companies in database"; //error code #3 - number id matching does not equal 1
		exit();
	}
	
	$companyInfo = mysqli_fetch_assoc($getCompanyValues);
	$companyValue = $companyInfo["value"];
	$companyLevel = $companyInfo["level"];
	
	echo "0\t".$companyValue."\t".$companyLevel;
?>