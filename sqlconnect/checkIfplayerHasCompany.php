<?php
	$con = mysqli_connect('localhost', 'root', 'root', 'IT_TYCOON');
	
	//check that connection happend
	if(mysqli_connect_errno())
	{
		echo "1: Connection failed"; //error code #1 = connection failed
		exit();
	}
	
	$userName = $_POST['UserName'];
	
	$playerNameCheckQuery = "SELECT Id FROM players WHERE Name='" . $userName . "';";
	
	$getPlayerId = mysqli_query($con, $playerNameCheckQuery) or die("2: Name check failed"); //error code #2 - name check query failed
	
	if(mysqli_num_rows($getPlayerId) != 1)
	{
		echo "3: Either no user with name, or more than one"; //error code #3 - number names matching does not equal 1
		exit();
	}
	
	$playerInfo = mysqli_fetch_assoc($getPlayerId);
	$playerId = $playerInfo["Id"];
	
	$playerHasCompanyQuery = "SELECT c.CompanyName, c.level FROM companytoplayer as ctp JOIN companies as c WHERE ctp.PlayerId = " . $playerId ." and ctp.CompanyId = c.Id;";
	
	$playerHasCompany = mysqli_query($con, $playerHasCompanyQuery) or die("6: Company Check failed"); //error code #2 - name check query failed
	
	if(mysqli_num_rows($playerHasCompany) != 1)
	{
		echo "7: Company does not exist"; //error code #3 - number names matching does not equal 1
		exit();
	}
	
	$companyInfo = mysqli_fetch_assoc($playerHasCompany);
	$companyName = $companyInfo["CompanyName"];
	$companyLevel = $companyInfo["level"];
	
	echo "0\t".$companyName."\t".$companyLevel;

?>