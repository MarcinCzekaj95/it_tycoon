<?php
	$con = mysqli_connect('localhost', 'root', 'root', 'IT_TYCOON');
	
	//check that connection happend
	if(mysqli_connect_errno())
	{
		echo "1: Connection failed"; //error code #1 = connection failed
		exit();
	}
	
	$id = $_POST['Id'];
	
	$employerIdChecker = "SELECT Id, Name, Surname, Age, Skill, Value FROM employees WHERE Id=" . $id . ";";
	
	$getEmployeeValues = mysqli_query($con, $employerIdChecker) or die("2: Id Check Failed"); //error code #2 - id check query failed
	
	if(mysqli_num_rows($getEmployeeValues) != 1)
	{
		echo "3: Either one or none of employees in database"; //error code #3 - number id matching does not equal 1
		exit();
	}
	
	$employeeInfo = mysqli_fetch_assoc($getEmployeeValues);
	$employeeId = $employeeInfo["Id"];
	$employeeName = $employeeInfo["Name"];
	$employeeSurname = $employeeInfo["Surname"];
	$employeeAge = $employeeInfo["Age"];
	$employeeSkill = $employeeInfo["Skill"];
	$employeeValue = $employeeInfo["Value"];
	
	echo "0\t".$employeeId."\t".$employeeName."\t".$employeeSurname."\t".$employeeAge."\t".$employeeSkill."\t".$employeeValue;

?>