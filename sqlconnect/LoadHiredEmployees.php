<?php
	$con = mysqli_connect('localhost', 'root', 'root', 'IT_TYCOON');
	
	//check that connection happend
	if(mysqli_connect_errno())
	{
		echo "1: Connection failed"; //error code #1 = connection failed
		exit();
	}
	
	$employeeId = $_POST['EmployeeId'];
	$companyName = $_POST['CompanyName'];
	
	$companyIdChecker = "SELECT Id FROM companies WHERE CompanyName='" . $companyName . "';";
	
	$getCompanyId = mysqli_query($con, $companyIdChecker) or die("2: Id Company Check Failed"); //error code #2 - id check query failed
	
	if(mysqli_num_rows($getCompanyId) != 1)
	{
		echo "3: Either one or none of employees in database"; //error code #3 - number id matching does not equal 1
		exit();
	}
	
	$companyInfo = mysqli_fetch_assoc($getCompanyId);
	
	$employeesToCompany = "SELECT E.Id, E.Name, E.Surname, E.Age, E.Skill, E.Value FROM employees E JOIN employeetocompnay ETC WHERE employeeId=" . $employeeId . " and companyId=". $companyInfo["Id"] . " and E.Id = ETC.employeeId;";
	
	$getHiredEmployee = mysqli_query($con, $employeesToCompany) or die("4: Id CompanyId (employeeToCompany) Check Failed"); //error code #4 - id check query failed
	
	if(mysqli_num_rows($getHiredEmployee) != 1)
	{
		echo "5: You don't have employee with this ID"; //error code #5 - number id matching does not equal 1
		exit();
	}
	
	$employeeInfo = mysqli_fetch_assoc($getHiredEmployee);
	$employeeId = $employeeInfo["Id"];
	$employeeName = $employeeInfo["Name"];
	$employeeSurname = $employeeInfo["Surname"];
	$employeeAge = $employeeInfo["Age"];
	$employeeSkill = $employeeInfo["Skill"];
	$employeeValue = $employeeInfo["Value"];
	
	echo "0\t".$employeeId."\t".$employeeName."\t".$employeeSurname."\t".$employeeAge."\t".$employeeSkill."\t".$employeeValue;
?>