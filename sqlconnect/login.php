<?php

	$con = mysqli_connect('localhost', 'root', 'root', 'IT_TYCOON');
	
	//check that connection happend
	if(mysqli_connect_errno())
	{
		echo "1: Connection failed"; //error code #1 = connection failed
		exit();
	}
	
	$username = $_POST['name'];
	$password = $_POST['password'];
	
	//check if name exists
	$nameCheckQuery = "SELECT Name, Salt, Hash, VirtualWallet, Wallet FROM players WHERE Name='" . $username . "';";
	
	$nameCheck = mysqli_query($con, $nameCheckQuery) or die("2: Name check failed"); //error code #2 - name check query failed
	
	if(mysqli_num_rows($nameCheck) != 1)
	{
		echo "5: Either no user with name, or more than one"; //error code #5 - number names matching does not equal 1
		exit();
	}
	
	//get login info from query
	$existingInfo = mysqli_fetch_assoc($nameCheck);
	$salt = $existingInfo["Salt"];
	$hash = $existingInfo["Hash"];
	
	$loginHash = crypt($password, $salt);
	if($hash != $loginHash)
	{
		echo "6: Incorrect password"; //error code #6 - password does not hash to match table
		exit();
	}
	
	echo "0\t" . $existingInfo["VirtualWallet"] . "\t" . $existingInfo["Wallet"];
	
?>