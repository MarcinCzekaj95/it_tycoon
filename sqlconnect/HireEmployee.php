<?php
	$con = mysqli_connect('localhost', 'root', 'root', 'IT_TYCOON');
	
	//check that connection happend
	if(mysqli_connect_errno())
	{
		echo "1: Connection failed"; //error code #1 = connection failed
		exit();
	}
	
	$id = $_POST['Id'];
	$companyName = $_POST['CompanyName'];
	
	$employerIdChecker = "SELECT Id, Name, Surname, Age, Skill, Value FROM employees WHERE Id=" . $id . ";";
	
	$getEmployeeValues = mysqli_query($con, $employerIdChecker) or die("2: Id Check Failed"); //error code #2 - id check query failed
	
	if(mysqli_num_rows($getEmployeeValues) != 1)
	{
		echo "3: Either one or none of employees in database"; //error code #3 - number id matching does not equal 1
		exit();
	}
	
	$companyNameChecker = "SELECT Id, CompanyName FROM companies WHERE CompanyName='" . $companyName . "';";
	
	$getCompanyValues = mysqli_query($con, $companyNameChecker) or die("4: CompanyName Check Failed"); //error code #4 - name check query failed
	
	if(mysqli_num_rows($getCompanyValues) != 1)
	{
		echo "5: one or many companies in database"; //error code #5 - number id matching does not equal 1
		exit();
	}
	
	$companyInfo = mysqli_fetch_assoc($getCompanyValues);
	$companyId = $companyInfo["Id"];
	
	$employeeInfo = mysqli_fetch_assoc($getEmployeeValues);
	$employeeId = $employeeInfo["Id"];
	$employeeName = $employeeInfo["Name"];
	$employeeSurname = $employeeInfo["Surname"];
	$employeeAge = $employeeInfo["Age"];
	$employeeSkill = $employeeInfo["Skill"];
	$employeeValue = $employeeInfo["Value"];
	
	$attachEmployeeToCompany = "INSERT INTO employeetocompnay (employeeId, companyId) VALUES(" . $employeeId . ", ".$companyId.");";
	
	mysqli_query($con, $attachEmployeeToCompany) or die("6: Attach Employee to company failed"); //error code #6 - insert query failed

	echo "0\t".$employeeId."\t".$employeeName."\t".$employeeSurname."\t".$employeeAge."\t".$employeeSkill."\t".$employeeValue;
?>